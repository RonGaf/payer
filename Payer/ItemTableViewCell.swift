//
//  ItemTableViewCell.swift
//  Payer
//
//  Created by Ron Gafni on 30/04/2018.
//  Copyright © 2018 Ron Gafni. All rights reserved.
//

import UIKit

protocol ItemTableViewCellDelegate {
    func didSelectCheckboxWith(item: Item)
    func didUpdateItem(with item: Item)
}

class ItemTableViewCell: UITableViewCell {

    @IBOutlet weak var checkboxButtonOutlet: UIButton!
    @IBOutlet weak var plusButtonOutlet: UIButton!
    @IBOutlet weak var minusButtonOutlet: UIButton!
    @IBOutlet weak var itemNameLabel: UILabel!
    
    @IBOutlet weak var nameAndAmountStackView: UIStackView!
    @IBOutlet weak var nameAndAmountStackViewCenterConstraint: NSLayoutConstraint!

    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var itemCostLabel: UILabel!
    
    public var didSelectItem: Bool = false
    
    var delegate: ItemTableViewCellDelegate?
    
    var item: Item? {
        didSet {
            
            if let itemData = item {
                
                self.itemNameLabel.text = itemData.itemName
                self.itemCount = itemData.itemCount
                self.itemCost = itemData.itemCost
                self.itemAmount = itemData.itemAmount
                self.totalAmountLabel.text = String(itemData.itemAmount)
                
            }
        }
    }
    
    var itemAmount  = 0
    var itemCount   = 0
    
    var itemCost = 0 {
        didSet {
            itemCostLabel.text = "\(itemCost)$"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        descriptionLabel.isHidden = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func plusButton(_ sender: Any) {
        
        if itemAmount >= itemCount + 1 {
            itemCount += 1
            
            item?.itemCount = itemCount
            descriptionLabel.text = "Pay \((itemCount) * itemCost)$ for \(itemCount)"
            delegate?.didUpdateItem(with: item!)
            
        }
    }
    
    @IBAction func minusButton(_ sender: Any) {
        
        if itemCount - 1 >= 1 {
            itemCount -= 1
            
            item?.itemCount = itemCount
            descriptionLabel.text = "Pay \((itemCount) * itemCost)$ for \(itemCount)"
            delegate?.didUpdateItem(with: item!)
        }
    }
    
    //MARK: - Action Methods
    
    @IBAction func didTapCheckbox(_ sender: Any) {
        
        if didSelectItem {
            
            checkboxButtonOutlet.setImage(UIImage(named:"empty_checkmark_icon"), for: .normal)
            descriptionLabel.isHidden = true
            
            minusButtonOutlet.isEnabled = false
            plusButtonOutlet.isEnabled = false
                        
            nameAndAmountStackViewCenterConstraint.constant = 0
            layoutIfNeededAnimate()
            
        } else {
            
            checkboxButtonOutlet.setImage(UIImage(named:"checkmark_icon"), for: .normal)
            descriptionLabel.isHidden = false
            
            minusButtonOutlet.isEnabled = true
            plusButtonOutlet.isEnabled = true
            
            item?.itemCount = itemCount
            descriptionLabel.text = "Pay \((itemCount) * itemCost)$ for \(itemCount)"
            
            nameAndAmountStackViewCenterConstraint.constant = 5
            layoutIfNeededAnimate()
            
        }
        
        delegate?.didSelectCheckboxWith(item: item!)
        
        didSelectItem = !didSelectItem
        item?.isSelected = didSelectItem

    }
    
    
    //MARK: Helper Methods
    
    func layoutIfNeededAnimate() {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.layoutIfNeeded()
            
        }) { (complete) in
            
        }
    }
    
}
