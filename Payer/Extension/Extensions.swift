//
//  Extensions.swift
//  Payer
//
//  Created by Ron Gafni on 29/01/2018.
//  Copyright © 2018 Ron Gafni. All rights reserved.
//

import UIKit

//MARK:- UIButton extension
extension UIButton {
    
    func setupPayerButton() {
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = 5.0
    }
    
    func setupCheckboxButton() {
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1.0
    }
}

//MARK:- UIColor extension

let mainColor = UIColor().colorWith(red: 24.0, green: 29.0, blue: 37.0, alpha: 1) //Dark Blue

extension UIColor {
    
    func colorWith(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: (red/255.0), green: (green/255.0), blue: (blue/255.0), alpha: alpha)
    }
}
