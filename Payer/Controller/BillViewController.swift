//
//  BillViewController.swift
//  Payer
//
//  Created by Ron Gafni on 01/05/2018.
//  Copyright © 2018 Ron Gafni. All rights reserved.
//

import UIKit

class BillViewController: UIViewController {

    @IBOutlet weak var billPayedLabel: UILabel!
    @IBOutlet weak var leftToPayLabel: UILabel!
    @IBOutlet weak var totalBillLabel: UILabel!
    @IBOutlet weak var selectedItemBillLabel: UILabel!
    
    @IBOutlet weak var payButtonOutlet: UIButton!
    
    @IBOutlet weak var customAmountTextField: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        setupButtons()
        setupTextFields()
        setupData()
        setupNotifications()
    }

    func setupButtons() {
        payButtonOutlet.setupPayerButton()
    }
    
    func setupTextFields() {
        customAmountTextField.layer.cornerRadius = 5.0
    }
    
    var itemsArray = [Item]()
    
    func setupData() {
        
        //Temp
        itemsArray  =  [Item(itemName: "Maacbi", itemCost: 29, itemAmount: 3, itemPayed: 0,itemCount: 0, itemId: "1"),
                        Item(itemName: "Calsberg", itemCost: 32, itemAmount: 5, itemPayed: 0,itemCount: 0, itemId: "2"),
                        Item(itemName: "Fries", itemCost: 15, itemAmount: 3, itemPayed: 0,itemCount: 0, itemId: "3"),
                        Item(itemName: "Burger Sandwich", itemCost: 29, itemAmount: 3, itemPayed: 0,itemCount: 0, itemId: "4")]
    }
    
    func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            view.frame.offsetBy(dx: 0, dy: +keyboardHeight)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            view.frame.offsetBy(dx: 0, dy: -keyboardHeight)
        }
    }
    
    //MARK: - Temp Setup Data
    
    var users = [User]()
    
    func setupUser() {
        
        itemsArray  =  [Item(itemName: "Maacbi", itemCost: 29,   itemAmount: 3, itemPayed: 0, itemCount: 0, itemId: "1"),
                        Item(itemName: "Calsberg", itemCost: 32, itemAmount: 5, itemPayed: 0, itemCount: 0, itemId: "2"),
                        Item(itemName: "Fries", itemCost: 15,    itemAmount: 3, itemPayed: 0, itemCount: 0, itemId: "3"),
                        Item(itemName: "Burger Sandwich",itemCost: 29, itemAmount: 3, itemPayed: 0, itemCount: 0, itemId: "4")]
        
        users.append(User(firstName: "Ron",     lastName: "Gafni",   userId: "1", payedStatusAmount: 0, profileImageURL: "", itemsPayed:[]))
        
        users.append(User(firstName: "Omer",    lastName: "Gafni",   userId: "2", payedStatusAmount: 0, profileImageURL: "", itemsPayed: []))
        users.append(User(firstName: "Eyal",    lastName: "Benisho", userId: "3", payedStatusAmount: 0, profileImageURL: "", itemsPayed: []))
        users.append(User(firstName: "Yarin",   lastName: "Dahaman", userId: "4", payedStatusAmount: 0, profileImageURL: "", itemsPayed: []))
        setupTable()
    }
    
    var table = Table(tableId: "12345", items: [], users: [], tableCode: "123456", tableHistory: nil, billAmount: 300, billPayed: 0)
    
    func setupTable() {
        table = Table(tableId: "12345", items: itemsArray, users: users, tableCode: "123456", tableHistory: nil, billAmount: 300, billPayed: 0)
        tableView.reloadData()
    }
    
    //MARK: - Bill Calculation Methods
    var finalBillAmount = 0
    var itemToPay = [Item]()
    
    func updateFinalBill(item: Item) {
        
        if let index = itemToPay.index(of: item) {
            itemToPay[index] = item
        }
        
        updateBill(amount: finalBillAmount)
    }
    
    func removeItem(item: Item) {
        if item.isSelected {
            if let index = itemToPay.index(of: item) {
                itemToPay.remove(at: index)
            }
        }else {
            itemToPay.append(item)
        }
        
        updateBill(amount: finalBillAmount)

    }
    
    func updateBill(amount: Int) {
        
        finalBillAmount = 0
        for item in itemToPay {
           finalBillAmount += item.totalCost()
        }
        
        selectedItemBillLabel.text = "Selected Items Bill: \(finalBillAmount)"

    }
    

}

extension BillViewController: UITableViewDataSource, UITableViewDelegate, ItemTableViewCellDelegate {
    
    //MARK: - UITableView Delegate & Data Source
    
    func setupTableView() {
        
        tableView.separatorColor = mainColor
        tableView.delegate   = self
        tableView.dataSource = self
        setupUser()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return table.items.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTableViewCellIdentifer", for: indexPath) as! ItemTableViewCell
        
        cell.item = table.items[indexPath.row]
        cell.checkboxButtonOutlet.addTarget(self, action: #selector(didTapCheckBoxItem(sender:)), for: .touchUpInside)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    //MARK:- ItemTableViewCellDelegate Methods
    
    func didUpdateItem(with item: Item) {
        updateFinalBill(item: item)
    }
    
    func didSelectCheckboxWith(item: Item) {
        removeItem(item: item)
    }
    
    @objc func didTapCheckBoxItem(sender: UIButton) {
        guard let cell = sender.superview?.superview as? ItemTableViewCell else{
            return
        }
        
        print(cell)
    }
}


