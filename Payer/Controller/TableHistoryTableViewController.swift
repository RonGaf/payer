//
//  TableHistoryTableViewController.swift
//  Payer
//
//  Created by Ron Gafni on 27/01/2019.
//  Copyright © 2019 Ron Gafni. All rights reserved.
//

import UIKit

class TableHistoryTableViewController: UITableViewController {

    var users = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUser()
        setupTableView()
    }

    func setupUser() {
        
        users.append(User(firstName: "Ron",     lastName: "Gafni",   userId: "1", payedStatusAmount: 0, profileImageURL: "",  itemsPayed: [Item(itemName: "Maacbi", itemCost: 29,   itemAmount: 3, itemPayed: 2, itemCount: 0, itemId: "1"),
                                                                                                                                           Item(itemName: "Calsberg", itemCost: 32, itemAmount: 5, itemPayed: 1, itemCount: 0, itemId: "2"),
                                                                                                                                           Item(itemName: "Fries", itemCost: 15,    itemAmount: 3, itemPayed: 3, itemCount: 0, itemId: "3")]))
        users.append(User(firstName: "Omer",    lastName: "Gafni",   userId: "2", payedStatusAmount: 55, profileImageURL: "", itemsPayed: [] ))
        users.append(User(firstName: "Eyal",    lastName: "Benisho", userId: "3", payedStatusAmount: 0, profileImageURL: "",  itemsPayed: []))
        users.append(User(firstName: "Yarin",   lastName: "Dahaman", userId: "4", payedStatusAmount: 0, profileImageURL: "",  itemsPayed: []))
        tableView.reloadData()
    }
    
    func setupTableView() {
        tableView.estimatedRowHeight = 80
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserHistoryTableViewCellIdentifer", for: indexPath) as! UserHistoryTableViewCell
        
        cell.user = users[indexPath.row]

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if let cell = tableView.cellForRow(at: indexPath) as? UserHistoryTableViewCell {
            if cell.isCellSelected {
                return 150
            }
        }
        
        return 80
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? UserHistoryTableViewCell else {
            return
        }
        
        guard let user = cell.user else {
            return
        }
        
        if user.itemsPayed.count == 0 {
            return
        }
        
        cell.isCellSelected = !cell.isCellSelected
        
        if cell.isCellSelected {
           cell.payedStatusLabel.text = user.itemPayedStatus()
        }else{
            cell.payedStatusLabel.text = user.userPayedStatus()
        }
        
        
        tableView.beginUpdates()
        tableView.endUpdates()
        
        
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
