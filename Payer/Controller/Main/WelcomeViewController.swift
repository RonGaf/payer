//
//  WelcomeViewController.swift
//  Payer
//
//  Created by Ron Gafni on 30/01/2018.
//  Copyright © 2018 Ron Gafni. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
  
    

    //MARK:- Constraint Interface
    @IBOutlet weak var hiLabelConstraint: NSLayoutConstraint!
    @IBOutlet weak var joinTableConstraint: NSLayoutConstraint!
    @IBOutlet weak var codeTextFieldConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var codeNumberTextField: UITextField!
    @IBOutlet weak var joinTableStackView: UIStackView!
    @IBOutlet weak var welcomeStackView: UIStackView!

    @IBOutlet weak var tablesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
       
    }

    //MARK:- Setup Buttons
    
    func setupUI() {
        setupButtons()
        setupTextFields()
    }
    
    func setupButtons() {
        
        enterCodeButtonOutlet.setupPayerButton()
        joinTableButtonOutlet.setupPayerButton()
    }
    
    func setupTextFields() {
        //codeNumberTextField.isHidden = true
        
        codeNumberTextField.layer.borderColor = UIColor.white.cgColor
        codeNumberTextField.layer.borderWidth = 1.0
        codeNumberTextField.layer.cornerRadius = 5.0
        
        
    }
    
    @IBOutlet weak var joinTableButtonOutlet: UIButton!
    @IBOutlet weak var enterCodeButtonOutlet: UIButton!
    
    @IBAction func joinTablePressed(_ sender: Any) {
        
        dismissWelcome()
    }
    
    func dismissWelcome() {
                
        UIView.animate(withDuration: 0.7, animations: {
            self.welcomeStackView.layer.opacity = 0
        }) { (finished) in
            self.welcomeStackView.isHidden = true
            self.showJoinTable()
        }
    }
    
    func showJoinTable() {
        UIView.animate(withDuration: 1, animations: {
            self.joinTableStackView.layer.opacity = 1
        }) { (finished) in
            self.joinTableStackView.isHidden = false

        }
    }
   
    
   /* //MARK:- UITableView Delegate & Data Source
    
    var tablesName = ["Ron Table", "Omer Friends", "", "", ""]
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
     */
}
