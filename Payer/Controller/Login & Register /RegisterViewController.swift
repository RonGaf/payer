//
//  RegisterViewController.swift
//  Payer
//
//  Created by Ron Gafni on 29/01/2018.
//  Copyright © 2018 Ron Gafni. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupButtons()
    }
    
    //MARK:- Setup Methods
    
    func setupButtons() {
        registerButtonOutlet.setupPayerButton()
    }
    
    @IBOutlet weak var registerButtonOutlet: UIButton!
    @IBAction func registerPressed(_ sender: Any) {
        
    }
}
