//
//  LoginViewController.swift
//  Payer
//
//  Created by Ron Gafni on 29/01/2018.
//  Copyright © 2018 Ron Gafni. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupButtons()
    }
    
    //MARK: - Setup Methods
    
    func setupButtons() {
        loginButtonOutlet.setupPayerButton()
    }
    
    @IBOutlet weak var loginButtonOutlet: UIButton!
    @IBAction func loginPressed(_ sender: Any) {
        
    }
}
