//
//  UserHistoryTableViewCell.swift
//  Payer
//
//  Created by Ron Gafni on 27/01/2019.
//  Copyright © 2019 Ron Gafni. All rights reserved.
//

import UIKit

class UserHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var payedStatusLabel: UILabel!
    @IBOutlet weak var backgroundColorView: UIView!
    
    var isCellSelected = false
    
    var user: User? {
        didSet {            
            if let user = user {
                usernameLabel.text    = user.fullName()
                payedStatusLabel.text = user.userPayedStatus()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()
    }


    func setupUI() {
        profileImageView.layer.cornerRadius = 5.0
        profileImageView.layer.borderColor = UIColor.white.cgColor
        profileImageView.layer.borderWidth = 0.8
        
        backgroundColorView.layer.cornerRadius = 5.0
        
        self.selectionStyle = .none
    }

}
