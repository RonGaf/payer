//
//  User.swift
//  Payer
//
//  Created by Ron Gafni on 27/01/2019.
//  Copyright © 2019 Ron Gafni. All rights reserved.
//

import UIKit

struct User {
    
    let firstName: String
    let lastName: String
    let userId: String
    let payedStatusAmount: Int
    let profileImageURL: String? //URL from web
    let itemsPayed: [Item]
    
    init(firstName: String,
         lastName: String,
         userId: String,
         payedStatusAmount: Int,
         profileImageURL: String,
         itemsPayed: [Item]) {
     
        self.firstName           = firstName
        self.lastName            = lastName
        self.userId              = userId
        self.payedStatusAmount   = payedStatusAmount
        self.profileImageURL     = profileImageURL
        self.itemsPayed          = itemsPayed
    }
    
    public func fullName() -> String {
        return "\(firstName) \(lastName)"
    }
    
    public func userPayedStatus() -> String {
      
        //We use this method status to the users only when the cell is collapse
        //see itemPayedStatus() for expand cell
        
        if payedStatusAmount > 0 {
            return "Payed: \(payedStatusAmount)"
        } else {
            return "Not payed yet"
        }
    }
    
    public func itemPayedStatus() -> String {
        
        var statusString = "Payed for:"
        for item in itemsPayed {
            statusString.append(" \(item.itemCount) \(item.itemName)")
        }
        
        return statusString
    }
    
    public func profileImage() -> String {
        //In future need to load from url
        return ""
        
    }

}
