//
//  TableHistory.swift
//  Payer
//
//  Created by Ron Gafni on 27/01/2019.
//  Copyright © 2019 Ron Gafni. All rights reserved.
//

import UIKit

struct TableHistory {

    let tableId: String
    let billAmount: Int
    let billPayed: Int
    let participents: [User]
    let items: [Item]
    
    init(tableId: String,
         billAmount: Int,
         billPayed: Int,
         participents: [User],
         items: [Item]) {
        
        self.tableId      = tableId
        self.billAmount   = billAmount
        self.billPayed    = billPayed
        self.participents = participents
        self.items        = items
        
    }
}
