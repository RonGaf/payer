//
//  Item.swift
//  Payer
//
//  Created by Ron Gafni on 24/01/2019.
//  Copyright © 2019 Ron Gafni. All rights reserved.
//

import UIKit

struct Item {

    var itemName: String
    var itemCost: Int   = 0
    var itemAmount: Int = 0
    var itemPayed: Int?
    var itemCount: Int  = 0
    var isSelected      = false
    var itemId: String
    
    init(itemName: String,
         itemCost: Int,
         itemAmount: Int,
         itemPayed: Int,
         itemCount: Int,
         itemId: String) {
       
        self.itemName    = itemName
        self.itemCost    = itemCost
        self.itemAmount  = itemAmount
        self.itemPayed   = itemPayed
        self.isSelected  = false
        self.itemId      = itemId
    }
    
    public func totalCost() -> Int {
        return (self.itemCount * self.itemCost)
    }
}

extension Item: Equatable {
    static func == (lhs: Item, rhs: Item) -> Bool {
        return lhs.itemId ==  rhs.itemId
    }
}
