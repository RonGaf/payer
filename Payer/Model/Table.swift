//
//  Table.swift
//  Payer
//
//  Created by Ron Gafni on 27/01/2019.
//  Copyright © 2019 Ron Gafni. All rights reserved.
//

import UIKit

struct Table {
   
    let tableId: String
    var items: [Item]
    let users: [User]
    let tableCode: String
    let tableHistory: [Item]?
    let billAmount: Int
    let billPayed: Int
    
    init(tableId: String,
         items: [Item],
         users: [User],
         tableCode: String,
         tableHistory: [Item]?,
         billAmount: Int,
         billPayed: Int) {
        
        self.tableId        = tableId
        self.items          = items
        self.users          = users
        self.tableCode      = tableCode
        self.tableHistory   = tableHistory
        self.billAmount     = billAmount
        self.billPayed      = billPayed
        
    }

    
}
